// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:formation_flutter/models/character.model.dart';
import 'package:formation_flutter/screens/character_details_screen/character_details.screen.dart';
import 'package:formation_flutter/screens/character_details_screen/characters_details.bloc.dart';
import 'package:get_it/get_it.dart';
import 'package:mockito/mockito.dart';

class MockCharacterDetailsBloc extends Mock implements ICharacterDetailsBloc {}

void main() {
  testWidgets('Test charaterdetails', (WidgetTester tester) async {

    var dataTest = Character(
      id: 1, 
      name: 'Rick Sanchez', 
      imageUrl: 'https://rickandmortyapi.com/api/character/avatar/1.jpeg'
    );
    var fakeBloc = MockCharacterDetailsBloc();
    when(fakeBloc.fetchCharacter(1)).thenAnswer((_) async => dataTest);

    GetIt.I.registerFactory<ICharacterDetailsBloc>(() => MockCharacterDetailsBloc());

    await tester.pumpWidget(MaterialApp(
      home: CharacterDetailsScreen(1))
    );

    final nameFinder = find.text(dataTest.name);

    expect(nameFinder, findsOneWidget);
  });
}
