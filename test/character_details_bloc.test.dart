import 'package:flutter_test/flutter_test.dart';
import 'package:formation_flutter/models/character.model.dart';
import 'package:formation_flutter/screens/character_details_screen/characters_details.bloc.dart';
import 'package:formation_flutter/utils/repository.service.dart';
import 'package:mockito/mockito.dart';

class MockRepository extends Mock implements IRepository {}

void main() {

  group('Character Details Bloc', () {

    test('Fetch character works', () async {

      // data
      var dataTest = Character(id: 1, name: 'Rick Sanchez', imageUrl: 'https://rickandmortyapi.com/api/character/avatar/1.jpeg');

      // Repository 
      var repository = MockRepository();

      // Tell mockito to fake a return when called
      when(repository.fetchCharacter(1))
        .thenAnswer((_) async => dataTest);
      
      // Create my bloc
      CharacterDetailsBloc myTestBloc = CharacterDetailsBloc(repository);
      
      // Ask to fetch
      await myTestBloc.fetchCharacter(1);
      
      // Test
      expect(myTestBloc.characterStream, emits(dataTest));

    });


  });

}