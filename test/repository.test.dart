import 'package:flutter_test/flutter_test.dart';
import 'package:formation_flutter/models/character.model.dart';
import 'package:formation_flutter/services/repository.impl.dart';
import 'package:formation_flutter/utils/http.service.dart';
import 'package:mockito/mockito.dart';

class MockHttpService extends Mock implements IHttpService {}

void main() {

  group('Repository implementation test', () {

    test('Fetch character works', () async {

      // data
      var dataTest = Character(id: 1, name: 'Rick Sanchez', imageUrl: 'https://rickandmortyapi.com/api/character/avatar/1.jpeg');

      // Mock 
      var mockService = MockHttpService();

      // Tell mockito to fake a return when called
      when(mockService.get('https://rickandmortyapi.com/api/character/1'))
        .thenAnswer((_) async => {
          'id': 1,
          'name': 'Rick Sanchez',
          'image': 'https://rickandmortyapi.com/api/character/avatar/1.jpeg'
        });
      
      // Create my Repository
      Repository r = Repository(service: mockService);
      
      // Ask to fetch
      Character returnedCharacter = await r.fetchCharacter(1);
      
      // Test
      expect(dataTest, returnedCharacter);
    });

  });

  

}