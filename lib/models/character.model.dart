class Character {

  final int id;
  final String name;
  final String imageUrl;

  @override
  String toString() {
    return "$id, $name, $imageUrl";
  }

  Character({
    this.id, 
    this.name,    
    this.imageUrl
  });

  factory Character.fromJson(Map<String, dynamic> json) {
    return Character(
      id: json['id'],
      name: json['name'],
      imageUrl: json['image']
    );
  }

}