import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:formation_flutter/screens/character_details_screen/characters_details.bloc.dart';
import 'package:formation_flutter/screens/character_list_screen/character_list.screen.dart';
import 'package:formation_flutter/screens/character_list_screen/characters_list.bloc.dart';
import 'package:formation_flutter/services/dio.service.dart';
import 'package:formation_flutter/services/repository.impl.dart';
import 'package:formation_flutter/utils/http.service.dart';
import 'package:formation_flutter/utils/repository.service.dart';
import 'package:formation_flutter/utils/translation_delegate.utils.dart';
import 'package:get_it/get_it.dart';

void main() {
  registerServiceLocator();
  runApp(MyApp());
}

void registerServiceLocator() {
  GetIt.I.registerSingleton<IHttpService>(HttpService());
  GetIt.I.registerSingleton<IRepository>(Repository(service: GetIt.I.get<IHttpService>()));

  GetIt.I.registerFactory<ICharacterDetailsBloc>(() => CharacterDetailsBloc(GetIt.I.get<IRepository>()));
  GetIt.I.registerFactory<CharactersListBloc>(() => CharactersListBloc(GetIt.I.get<IRepository>()));
}

class MyApp extends StatelessWidget {

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
          theme: ThemeData(
                primarySwatch: Colors.blue,
                visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          localizationsDelegates: [
                const TranslationsDelegate(),
                GlobalMaterialLocalizations.delegate,
                GlobalWidgetsLocalizations.delegate,
          ],
          supportedLocales: [
                const Locale('en'),
                const Locale('fr'),
          ],
          home: CharacterListScreen()
    );
  }

}
