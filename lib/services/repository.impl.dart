import 'package:flutter/widgets.dart';
import 'package:formation_flutter/models/character.model.dart';
import 'package:formation_flutter/utils/http.service.dart';
import 'package:formation_flutter/utils/repository.service.dart';

class Repository implements IRepository {

  final IHttpService service;

  Repository({ 
    @required this.service
  });

  @override
  Future<Character> fetchCharacter(int characterId) async {
    var characterDataMap = await service.get('https://rickandmortyapi.com/api/character/$characterId');
    return Character.fromJson(characterDataMap);
  }

  @override
  Future<List<Character>> fetchCharacters() async {
    var characterListMapResponse = await service.get('https://rickandmortyapi.com/api/character/');
    var jsonCharacterList = characterListMapResponse['results'];
    return jsonCharacterList
        .map<Character>((jsonCharacter) => Character.fromJson(jsonCharacter))
        .toList();
  }

}