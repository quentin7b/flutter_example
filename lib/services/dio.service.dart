import 'package:dio/dio.dart';
import 'package:formation_flutter/utils/http.service.dart';

class HttpService implements IHttpService {
  @override
  Future<Map<String, dynamic>> get(String url) async {
    try {
      var response = await Dio().get(url);
      return response.data;
    } on DioError catch(e) {
        throw Exception(e.error.toString());
    }
  }

}