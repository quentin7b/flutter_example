import 'dart:convert';
import 'dart:ui';

import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';

class Translations {

    static Map<dynamic, dynamic> _localizedValues;
    Locale locale;
  
    Translations(Locale locale) {
        this.locale = locale;
        _localizedValues = null;
    }

    get currentLanguage => locale.languageCode;

    String text(String key)  =>  _localizedValues[key] ?? '** $key not found';

    static Translations of(BuildContext context) => Localizations.of<Translations>(context, Translations);

    static Future<Translations> load(Locale locale) async {
        Translations translations = new Translations(locale);
        String jsonContent = await rootBundle.loadString("locale/i18n_${locale.languageCode}.json");
        _localizedValues = json.decode(jsonContent);
        return translations;
    }

}
