import 'package:formation_flutter/models/character.model.dart';

abstract class IRepository {

  Future<List<Character>> fetchCharacters();

  Future<Character> fetchCharacter(int id);

}
