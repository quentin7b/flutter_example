abstract class IHttpService {

  Future<Map<String, dynamic>> get(String url);

}