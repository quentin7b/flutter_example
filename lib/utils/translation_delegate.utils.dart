import 'package:flutter/widgets.dart';
import 'package:formation_flutter/utils/translations_utils.dart';

class TranslationsDelegate extends LocalizationsDelegate<Translations> {


    const TranslationsDelegate(); // Constructor.

    @override
    bool isSupported(Locale locale) => ['en','fr'].contains(locale.languageCode);

    @override
    Future<Translations> load(Locale locale) => Translations.load(locale);

    @override
    bool shouldReload(TranslationsDelegate old) => false;
}
