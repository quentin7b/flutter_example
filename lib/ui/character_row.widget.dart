import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:formation_flutter/models/character.model.dart';
import 'package:formation_flutter/ui/aligned_text.widget.dart';
import 'package:formation_flutter/ui/character_picture.widget.dart';

class CharacterRow extends StatelessWidget {

  final Character character;

  CharacterRow(this.character);

  @override
  Widget build(BuildContext context) {
   return Container(
      margin: EdgeInsets.only(top: 4, bottom: 4, left: 8, right: 8),
      child: Row(
      children: [
        CharacterPicture(character.imageUrl),
        AlignedText('${character.name}')
      ],
    ));
  }
}