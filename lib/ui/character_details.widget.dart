import 'package:flutter/material.dart';
import 'package:formation_flutter/models/character.model.dart';

class CharacterDetailsWidget extends StatelessWidget {

  final Character character;

  CharacterDetailsWidget(this.character);

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Image.network(character.imageUrl)
      );
  }


  
}