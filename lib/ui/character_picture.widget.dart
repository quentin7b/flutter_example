import 'package:flutter/widgets.dart';

class CharacterPicture extends StatelessWidget {

  final String imageUrl;

  const CharacterPicture(this.imageUrl);

  @override
  Widget build(BuildContext context) {
    return Container(width: 48, height: 48, child: Image.network(imageUrl),);
  }
}