import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class AlignedText extends StatelessWidget {

  final String text;

  const AlignedText(this.text);

  @override
  Widget build(BuildContext context) {
    return Expanded(                
      flex: 1,
      child: Container(
        margin: EdgeInsets.only(left: 16),
        child: Text(text)
      ),
    );
  }

}