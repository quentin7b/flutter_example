
import 'dart:async';

import 'package:formation_flutter/bloc.dart';
import 'package:formation_flutter/models/character.model.dart';
import 'package:formation_flutter/utils/repository.service.dart';

class CharactersListBloc implements BLoC {

  final IRepository repository;

  CharactersListBloc(this.repository);  

  StreamController<List<Character>> _controller = StreamController();

  Stream<List<Character>> get charactersStream => _controller.stream;

  Future fetchCharacters() async {
    List<Character> characterList = await repository.fetchCharacters();
    _controller.sink.add(characterList);
  }

  @override
  void dispose() {
    _controller.close();
  }

}