import 'package:flutter/material.dart';
import 'package:formation_flutter/models/character.model.dart';
import 'package:formation_flutter/screens/character_details_screen/character_details.screen.dart';
import 'package:formation_flutter/screens/character_list_screen/characters_list.bloc.dart';
import 'package:formation_flutter/ui/character_row.widget.dart';
import 'package:formation_flutter/utils/translations_utils.dart';
import 'package:get_it/get_it.dart';

class CharacterListScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return CharacterListState(bloc: GetIt.I.get<CharactersListBloc>());
  }
}

class CharacterListState extends State<CharacterListScreen> {

  final CharactersListBloc bloc;

  CharacterListState({@required this.bloc});

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback((_) async {
      print(bloc);
      await bloc.fetchCharacters();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print(bloc);
    return StreamBuilder(
      stream: bloc.charactersStream,
      builder: (BuildContext context, AsyncSnapshot<List<Character>> snapshot) {
        if (snapshot.hasError) {
          print(snapshot.error);
          return Container(color: Colors.red);
        }
        else if (snapshot.hasData) {

          var characters = snapshot.data;
          return Scaffold(
            appBar: AppBar(title: Text(Translations.of(context).text('TITLE'))),
            body: ListView.builder(
            itemCount: characters.length,
            itemBuilder: (BuildContext ctxt, int index) {
              return InkWell(
                onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => CharacterDetailsScreen(characters[index].id))),
                child: CharacterRow(characters[index])
              );
            }));

        }
        else {
          // Loading
          return Container(color: Colors.blue);
        }
      });
  }

  @override
  void dispose() {
    bloc.dispose();
    super.dispose();
  }
}