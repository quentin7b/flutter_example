import 'package:flutter/material.dart';
import 'package:formation_flutter/models/character.model.dart';
import 'package:formation_flutter/screens/character_details_screen/characters_details.bloc.dart';
import 'package:formation_flutter/ui/character_details.widget.dart';
import 'package:get_it/get_it.dart';

class CharacterDetailsScreen extends StatefulWidget {

  final int characterId;

  const CharacterDetailsScreen(this.characterId);

  @override
  State<StatefulWidget> createState() {
    return CharacterDetailsState();
  }

}

class CharacterDetailsState extends State<CharacterDetailsScreen> {

  ICharacterDetailsBloc _bloc = GetIt.I.get<ICharacterDetailsBloc>();

  @override
  void initState() {
    _bloc.fetchCharacter(widget.characterId);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {  
    return StreamBuilder(
      stream: _bloc.characterStream,
      builder: (BuildContext context, AsyncSnapshot<Character> snap)  {
        if (snap.hasError) {
          return Container(color: Colors.red);
        } else if (snap.hasData) {
          return Scaffold(
            appBar: AppBar(title: Text(snap.data.name)),
            body: CharacterDetailsWidget(snap.data)
          );
        } else {
          return Container(color: Colors.blue);
        }
      }
    );
  }

}