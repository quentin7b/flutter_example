
import 'dart:async';

import 'package:formation_flutter/bloc.dart';
import 'package:formation_flutter/models/character.model.dart';
import 'package:formation_flutter/utils/repository.service.dart';

abstract class ICharacterDetailsBloc extends BLoC {

  Stream<Character> get characterStream;

  Future<void> fetchCharacter(int characterId);
}

class CharacterDetailsBloc implements ICharacterDetailsBloc {

  final IRepository repository;

  StreamController<Character> _controller = StreamController();

  @override
  Stream<Character> get characterStream => _controller.stream;

  CharacterDetailsBloc(this.repository);

  @override
  Future<void> fetchCharacter(int characterId) async {
    var currentCharacter = await repository.fetchCharacter(characterId);
    _controller.sink.add(currentCharacter);
  }

  @override
  void dispose() {
    _controller.close();
  }

}